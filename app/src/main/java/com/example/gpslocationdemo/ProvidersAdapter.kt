//package com.example.gpslocationdemo
//
//import android.content.Context
//import android.graphics.Color
//import android.view.View
//import android.view.ViewGroup
//import android.widget.ArrayAdapter
//import android.widget.TextView
//
//class ProvidersAdapter(context: Context, objects: List<Any?>) : ArrayAdapter<Any?>(
//    context,
//    android.R.layout.simple_list_item_2,
//    android.R.id.text1,
//    objects
//) {
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//        val view = super.getView(position, convertView, parent)
//
//        val text1 = view.findViewById<View>(android.R.id.text1) as TextView
//        val text2 = view.findViewById<View>(android.R.id.text2) as TextView
//
//        val provider = objects[position]
//        text1.text = provider
//        if (locationManager.isProviderEnabled(provider)) {
//            text2.text = "Enabled"
//            text2.setTextColor(Color.parseColor("#00FF00"));
//        } else {
//            text2.text = "Disabled"
//            text2.setTextColor(Color.parseColor("#FF0000"));
//        }
//        return view
//    }
//}