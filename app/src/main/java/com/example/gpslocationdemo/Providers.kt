package com.example.gpslocationdemo

data class Providers(
    val name: String,
    val isEnabled: Boolean
)
